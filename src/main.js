import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './components/Home'
import Feed from './components/Feed'

Vue.use(VueRouter)
var App = Vue.extend({})
var router = new VueRouter()
router.map({
  '/home': {
    component: Home
  },
  '/feed': {
    component: Feed
  }
})
router.start(App, '#app')
